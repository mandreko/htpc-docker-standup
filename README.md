# HTPC Docker Standup
---
This is a simple docker-compose configuration to standup a new HTPC. It's based on running on an Ubuntu server, but could easily be adapted for other opertaing systems with Docker support.

It includes the following Services

- [Plex Media Server](https://www.plex.tv/) - for managing media and serving files to Plex Clients
- [Deluge](https://deluge-torrent.org/) + [Private Internet Access](https://www.privateinternetaccess.com/pages/buy-vpn/toz) or [TorGuard](https://torguard.net/aff.php?aff=4350) - for downloading torrents... "safely"
- [NZBGet](https://nzbget.net/) - for downloading NZBs
- [Sonarr](https://sonarr.tv/) - for TV Series Management
- [Radarr](https://radarr.video/) - for Movie Management
- [Ombi](https://ombi.io/) - for shared content requests
- [Tautulli](http://tautulli.com/) - for Plex library statistics and usage
- [Watchtower](https://github.com/v2tec/watchtower) - for automatically updating running containers
- [InfluxDB](https://www.influxdata.com/) - for time series based database storage
- [Chronograf](https://www.influxdata.com/time-series-platform/chronograf/) - for making pretty dashboards out of the database data
- [SpeedTest](https://github.com/sivel/speedtest-cli/) - for performing a speedtest and posting data to the database
- [Traefik](https://traefik.io/) - for easily accessing services on intranet Hostnames
- [Docker-GC](https://github.com/clockworksoul/docker-gc-cron) - for cleaning up outdated Docker images

This project was forked from the [HTPC-Docker-Standup](https://gitlab.com/phikai/htpc-docker-standup) project... Many Thanks!

## Known Issues
- [ ] Sometimes the Deluge + VPN Container disconnects and can't re-establish a forwarded port connection. 


## Install Instructions

### Prerequisites
- [Ubuntu 16.04 LTS or 18.04 LTS](https://www.ubuntu.com/)
- VPN Account from [PIA](https://www.privateinternetaccess.com/pages/buy-a-vpn/1218buyavpn?invite=U2FsdGVkX1_CnFzYm0Qv6-hQrtggAyOlH4FYRGLUKu8%2C886hk0ioHSyrU1xcEtvWoneQyd4) or [TorGuard](https://torguard.net/aff.php?aff=4350)
- [Git](https://git-scm.com/)
- [Docker](https://www.docker.com/)
- [Python 2.7](https://www.python.org/)
- [Docker-Compose](https://docs.docker.com/compose/)

### Server Configuration
1. [Install Ubuntu Server](https://tutorials.ubuntu.com/tutorial/tutorial-install-ubuntu-server) and get it updated - `sudo apt update` then `sudo apt upgrade`
2. If you have existing media on a network share:
 - Mount NFS/CIFS shares locally:
 - Edit the /etc/fstab file for each share you want to mount
    - CIFS Example: `//10.0.50.2/Movies /mnt/Movies cifs credentials=/etc/.smbcredentials 0 0`
        - Create `/etc/.smbcredentials` file as shown:
        ```
        username=username_goes_here
        password=password_goes_here
        ```
        - `sudo chown root:root /etc/.smbcredentials; sudo chmod 600 /etc/.smbcredentials` - Set file permissions on `/etc/.smbcredentials`
    - NFS Example: `nas.home.andreko.net:/volume1/Docker    /mnt/Docker     nfs    auto,noatime,nolock,bg,intr,tcp,actimeo=1800 0 0`
 - `sudo mkdir /media/Media` - make the mount point
3. [Install Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository) & [Docker Compose](https://docs.docker.com/compose/install/#install-compose)
 - You'll need to add your user running docker to the `docker` group. See [Post-installation steps for Linux](https://docs.docker.com/install/linux/linux-postinstall/)
    ```
    sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sudo apt-key fingerprint 0EBFCD88
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sudo apt update
    sudo apt upgrade
    sudo apt install -y docker-ce
    sudo curl -L "https://github.com/docker/compose/releases/download/1.23.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
    sudo chmod +x /usr/local/bin/docker-compose
    ```
4. Clone Repo `git clone https://gitlab.com/mandreko/htpc-docker-standup.git`
5. Copy Sample Environment File `cp sample.env .env`
6. Edit `.env` to match your environment
7. Move the `arch_deluge` and `chronograf` folders to your `${CONFIG_PATH}` (initial run only)
7. Run Docker Environment `docker-compose up -d`
8. ???
9. Profit.

### Environment File
- `DOMAIN=` - Public domain to use for accessing services via a public domain - `homelab.domain.com`
- `IP_ADDRESS=` - Local IP Address of the server, should be static
- `CIDR_ADDRESS=` - IP/netmask entries which allow access to the server without requiring authorization. We recommend you set this only if you do not sign in your server. For example `192.168.1.0/24,172.16.0.0/16` will allow access to the entire `192.168.1.x` range and the `172.16.x.x`
- `EMAIL=` - Email address used for LetsEncrypt certificates
- `CONFIG_PATH` - Path to store all of your docker configuration
- `TV_PATH` - Path to store all of your TV episodes
- `MOVIES_PATH` - Path to store all of your Movies
- `TRANSCODE_PATH` - Path to store the Plex Transcode files (suggested to be a local mount, even in /tmp)
- `DOWNLOAD_PATH` - Path to store all of your temporarily downloaded files
- `PUID=` - UID of the local user, can be found by executing `id` from the command line
- `PGID=` - GID of the local user, can be found by executing `id` from the command line
- `TZ=` - Set the timezone inside the container. For example: `Europe/London`. The complete list can be found here: [https://en.wikipedia.org/wiki/List_of_tz_database_time_zones](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones)
- `PLEX_TAG=` - Which version of Plex Media Server, available options: `latest`, `beta` and `public`. You should use `public` unless you are a Plex Pass account
- `PLEX_HOSTNAME=` - Hostame of the server, can be found by executing `hostname` from command line
- `PLEX_CLAIM=` - The claim token for the server to obtain a real server token. If not provided, server is will not be automatically logged in. If server is already logged in, this parameter is ignored. You can obtain a claim token to login your server to your plex account by visiting [https://www.plex.tv/claim](https://www.plex.tv/claim)
- `VPN_USER=` - Your VPN username from [PIA](https://www.privateinternetaccess.com/pages/buy-a-vpn/1218buyavpn?invite=U2FsdGVkX1_CnFzYm0Qv6-hQrtggAyOlH4FYRGLUKu8%2C886hk0ioHSyrU1xcEtvWoneQyd4) or [TorGuard](https://torguard.net/aff.php?aff=4350)
- `VPN_PASS=` - Your VPN password from [PIA](https://www.privateinternetaccess.com/pages/buy-a-vpn/1218buyavpn?invite=U2FsdGVkX1_CnFzYm0Qv6-hQrtggAyOlH4FYRGLUKu8%2C886hk0ioHSyrU1xcEtvWoneQyd4) or [TorGuard](https://torguard.net/aff.php?aff=4350)
- `VPN_PROVIDER=` - Your VPN provider, name must match a folder specified in `ovpn`. This defaults to `pia` if you copied `sample.env`.
- `VPN_DNS_SERVERS` - The DNS servers that will be used once connected to the VPN (Google's: 8.8.8.8)
- `WATCHTOWER_NOTIFICATION_EMAIL_FROM=` - From address that your SMTP server uses to send email - `someone@somewhere.com`
- `WATCHTOWER_NOTIFICATION_EMAIL_TO=` - Email address you'd like Watchtower to notify for any notifications - `someone@somewhere.com`
- `WATCHTOWER_NOTIFICATION_EMAIL_SERVER=` - Servername of your SMTP server - `smtp.domain.com`
- `WATCHTOWER_NOTIFICATION_EMAIL_SERVER_PORT=` - Port that your SMTP server uses to connect - `587`
- `WATCHTOWER_NOTIFICATION_EMAIL_SERVER_USER=` - Username that your SMTP server uses to authenticate
- `WATCHTOWER_NOTIFICATION_EMAIL_SERVER_PASSWORD=` - Password for your SMTP user to authenticate
- `SPEEDTEST_INTERVAL=` - Number of seconds between tests to the [Speedtest.net](http://www.speedtest.net/) services

### Email/SMTP Service
- [Mailgun](https://documentation.mailgun.com/en/latest/quickstart.html) has an excellent QuickStart Guide
- Check out the sending via [SMTP](https://documentation.mailgun.com/en/latest/quickstart-sending.html#send-via-api)
- Make sure to also [verify your domain](https://documentation.mailgun.com/en/latest/quickstart-sending.html#verify-your-domain)

---

## Tips and Tricks
- [PIA List of Servers](https://helpdesk.privateinternetaccess.com/hc/en-us/articles/219460187-How-do-I-enable-port-forwarding-on-my-VPN-) that support port forwarding
- [Mullvad Servers](https://mullvad.net/en/servers/)
- [Deluge + PIA FAQ](https://lime-technology.com/forums/topic/44108-support-binhex-general/?tab=comments#comment-433613)
- [Removing Old/Completed Torrents from Deluge](https://www.cuttingcords.com/home/2015/2/4/auto-deleting-finished-torrents-from-deluge)
- [Create Series Folder in Sonarr](https://forums.sonarr.tv/t/adding-new-series-path-issues/2751/2)
- Open Shell in a Container - [Link 1](http://phase2.github.io/devtools/common-tasks/ssh-into-a-container/), [Link 2](https://stackoverflow.com/a/30173220)

---

If this project has helped you in anyway, and you'd like to say thanks...

[![Donate](https://img.shields.io/badge/Donate-SquareCash-brightgreen.svg)](http://cash.me/$PasswordReset)
[![Donate with Bitcoin](https://en.cryptobadges.io/badge/micro/3EeP9K9fn4uHpCQrXQvKndJvSdNKykHeGt)](https://en.cryptobadges.io/donate/3EeP9K9fn4uHpCQrXQvKndJvSdNKykHeGt)

_AFFILIATE DISCLOSURE: You can also support this project by purchasing a VPN Subscription via one of the links in this README._

---

# Disclaimer

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
